#!/usr/bin/env bash

BASE_DIR=/opt/projects/epam
VERSION=1.0-SNAPSHOT

mvn install:install-file \
    -DgroupId=org.apache \
    -DartifactId=bitemporal \
    -Dversion=${VERSION} \
    -Dfile=${BASE_DIR}/bitemporal/target/bitemporal-${VERSION}.jar \
    -Dpackaging=jar \
    -DcreateChecksum=true \
    -DpomFile=${BASE_DIR}/bitemporal/pom.xml \
    -DgeneratePom=true \
    -DlocalRepositoryPath=./repository 

